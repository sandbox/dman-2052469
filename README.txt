Region Visibility
=================

This module allows page specific visibility settings, similar to how the 
visibility settings work when configuring a block.  

Installation
------------
To install with the module, simply enable, configure permissions, 
and then visit the block settings pages at
  admin/structure/block
'Regions' on that page will now be linked, and region settings can be 
edited from there. 
Settings are different per-theme.